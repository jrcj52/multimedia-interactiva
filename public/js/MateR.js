function CrearGenerador(pos, rot)
{
	var Nuevo= Mallas["Generador"]
	
	Nuevo.position.set(pos[0], pos[1], pos[2])
	Nuevo.rotation.set(rot[0], rot[1], rot[2])
	Nuevo.scale.set(10,10,10)
	
	Objetos.push(Nuevo)
	scene.add(Nuevo)
	Generador = Nuevo
	Generador.CNT=0;
	Generador.CNT2=0;
	Generador.CNT3=0;
	Generador.CNT4=0;
	Generador.Estado="ESPERA"
	Generador.Accion=function()
	{
		this.children[0].rotation.y+=0.1*delta
		if(this.Estado=="Activo")
		{
			this.CNT+=delta;
			if(this.CNT>1)
			{
				this.CNT2+=delta
				if(this.CNT2 < 4)
					this.children[2].rotation.z+=0.2*delta
				else
				{
					this.CNT3+=delta
					if(this.CNT3 <3)
						this.rotation.y+=0.1*delta
					else
					{
						this.CNT4+=delta
						if(this.CNT4>2)
						{
							NivelJuego=1
							alert("Teranium y sus secuaces fue derrotado ya hora el planeta gozara nuevamente de su gran fuente de energía y vida")
							location.reload()
						}
					}
				}
			}
		}
	}
}

Muertos=0
n=false;
function RobotMuerto()
{
	if(n)
		return
	Muertos++
	if(Muertos>=3)
	{
		//console.log("Matando todo")
		n=true
		controls.getObject().Vida+=20
		for(i=0; i<Objetos.length; i++)
		{
			if(Objetos[i].Impacto)
			{
				Objetos[i].Estado="Activo"
				Objetos[i].Impacto(20)
			}
			Generador.Estado="Activo"
		}
		
	}
	//console.log("-"+Muertos)
}

function CrearTeranium(pos, rot, esc, dat)
{
	var Nuevo= NuevoEnemigoRobot(pos, rot, dat)
	//var Nuevo = Mallas["Robot"].clone()
	Nuevo.position.set	(pos[0], pos[1]+1.8, pos[2])
	Nuevo.rotation.set	(rot[0], rot[1], rot[2])
	Nuevo.scale.set		(2.5,2.5,2.5)
	Nuevo.Vida=10
	Nuevo.Distancia=60;
	Nuevo.Muerte=RobotMuerto
	Nuevo.Impacto2=function()
	{
		var Spawn = NuevoEnemigoRobot([this.position.x, this.position.y+10, this.position.z],[0,0,0])
		Spawn.Muerte=RobotMuerto
		//console.log(Spawn)
	}
}
function NuevoElevador(pos, rot, dat)
{
	var N = NuevoSuelo(pos, rot, dat)
	N=NuevoSuelo(e.pos, e.rot, e.dat)
	N.Activado=false
	N.Recorrido=0;
	N.Max=e.dat.Max
	N.Accion=function()
	{
		if(this.Activado)
		{
			this.Recorrido+=10*delta
			if(this.Recorrido<this.Max)
				this.position.y+=10*delta
		}
		
		if(this.position.distanceTo(controls.getObject().position) < 7)
			this.Activado=true;
	}
	return N
}
function NuevoSuelo(pos, rot, dat)
{	
	var Nuevo = new THREE.Mesh
	(
		new THREE.BoxGeometry(dat.X, dat.Y, dat.Z),
		M_Suelo
	)
	Nuevo.position.set(pos[0], pos[1], pos[2])
	Nuevo.rotation.set(rot[0], rot[1], rot[2])
	
	//Orilla en -Z
	Orilla=new THREE.Mesh
	(
		new THREE.BoxGeometry(dat.X+0.5, dat.Y+0.5, 1),
		M_Suelo_Orilla
	)
	Nuevo.add(Orilla)
	Orilla.position.z=-dat.Z/2;
	
	//Orilla en Z
	Orilla=new THREE.Mesh
	(
		new THREE.BoxGeometry(dat.X+0.5, dat.Y+0.5, 1),
		M_Suelo_Orilla
	)
	Nuevo.add(Orilla)
	Orilla.position.z=dat.Z/2;
	
	//Orilla en -X
	Orilla=new THREE.Mesh
	(
		new THREE.BoxGeometry(1, dat.Y+0.5, dat.Z+0.5),
		M_Suelo_Orilla
	)
	Nuevo.add(Orilla)
	Orilla.position.x=-dat.X/2;
	
	//Orilla en X
	Orilla=new THREE.Mesh
	(
		new THREE.BoxGeometry(1, dat.Y+0.5, dat.Z+0.5),
		M_Suelo_Orilla
	)
	Nuevo.add(Orilla)
	Orilla.position.x=dat.X/2;
	
	Objetos.push(Nuevo)
	scene.add(Nuevo)
	
	return Nuevo
}
velocidadRobotAtaque=200
velocidadRobot=35
function NuevoEnemigoRobot(pos,rot, Distancia)
{
	var Nuevo=Mallas["Robot"].clone()
	Nuevo.position.set(pos[0], pos[1]+1.8, pos[2])
	Nuevo.rotation.set(rot[0], rot[1], rot[2])
	
	Objetos.push(Nuevo)
	scene.add(Nuevo)
	
	Nuevo.Sen=Math.random()*2*Math.PI;
	Nuevo.Estado="Inactivo"
	Nuevo.Vida=5;
	Nuevo.Retraso=0
	Nuevo.TD=0;
	Nuevo.Distancia=90
	
	Recursion(Nuevo, Nuevo)
	
	Nuevo.Impacto=function(Danio)
	{
		if(this.Impacto2)
			this.Impacto2();
		
		if(this.Vida <=0)
			return;
		
		if(this.Estado=="Inactivo")
			this.Estado="Activo";
		
		this.Vida-=Danio;
		if(this.Vida <= 0)
		{
			//Creando explosion
			Proyectiles.push(Explosion
			(
				new THREE.Vector3().copy(this.position).add(new THREE.Vector3(0,5,0)),
				0xff8800
			))
			this.Vida=0;
			this.CDT=0;//Contador de tiempo
			if(this.Muerte)
				this.Muerte()
			console.log("Robot muerto")
		}
	}
	Nuevo.Accion=function()
	{
		if(this.Vida <= 0)
		{
			//Secuencia de muerte
			this.position.y-=20*delta;
			this.rotation.x+=delta*3.141516;
			this.rotation.z+=delta*3.141516/3;
			
			this.CDT+=delta;
			if(this.CDT>=2)
			{
				scene.remove(this)
				return true
			}
			
			return false
		}
		if(this.Estado=="Inactivo")
		{
			//El robot esta haciendo nada
			this.position.y-=Math.sin(this.Sen)
			this.Sen+=delta*2.5
			while(this.Sen>Math.PI*2)
				this.Sen-=Math.PI*2
			this.position.y+=Math.sin(this.Sen)
			
			if(this.position.distanceTo(controls.getObject().position) < this.Distancia)
				this.Estado="Activo"
			
			return false
		}
		if(this.Estado=="Atacando")
		{
			//El robot vuela hacia arriba de el
			Desplazamiento = new THREE.Vector3().copy(this.Direccion).multiplyScalar(delta * velocidadRobotAtaque)
			this.Recorrido+=Desplazamiento.length();
			
			if(this.Recorrido < this.Distacia)
				this.position.add(Desplazamiento)
			else
			{
				//Ya llego arriba
				this.position.copy(this.Destino)
				this.Estado="Callendo"
				this.Retraso=0.2
				this.Destino.y-=10
				this.Recorrido=0
			}
			
			return false
		}
		if(this.Estado == "Callendo")
		{
			//Esta callendo y golpeara al jugador
			this.Retraso-=delta;
			if(this.Retraso<0)
			{
				this.Recorrido+=delta*velocidadRobotAtaque;
				
				if(this.Recorrido < 10)
					this.position.y-=delta*velocidadRobotAtaque
				else
				{
					//Ya golpeo
					Radio=this.position.distanceTo(controls.getObject().position)
					if(Radio < 10)
					{
						//Le pego al jugador
						//console.log(Radio)
						
						if(Radio>1.4)	//Empujarlo fuera de la zona de impacto
						{
							Direccion = new THREE.Vector3().copy(controls.getObject().position).sub(this.position);
							controls.getObject().Impacto(2, Direccion, 180)
							
							//Empuje.add(Direccion)
							//console.log("Para afuera")
						}
						else			//Empujarlo para atras
						{
							velocity.z += 10000 * delta
							controls.getObject().Impacto(2)
							//console.log("Para atras")
						}
					}

					this.Retraso=0.5
					this.position.copy(this.Destino)
					this.Estado="Activo"
				}
				
			}
			return false
		}
		this.Retraso-=delta;
		if(this.Retraso>0)
			return false;
		
		//Tiempo entre ataques
		this.TD-=delta;
		if(this.TD<0)
			this.TD=0;
		
		if(this.TD<=0)
		{
			//El jugador esta sercas de el
			if(this.position.distanceTo(controls.getObject().position) < 50)
			{
				this.Estado="Atacando"
				this.Destino=new THREE.Vector3().copy(controls.getObject().position)
				this.Destino.y+=10
				
				this.Direccion=new THREE.Vector3().copy(this.Destino).sub(this.position).normalize()
				this.Distacia=this.Destino.distanceTo (this.position)
				this.Recorrido=0;
				this.TD=3;
				return false
			}
		}
		
		//Caminado hacia el objetivo
		
		if(this.position.distanceTo(controls.getObject().position) > 5)
		{
			Direccion=new THREE.Vector3().copy(controls.getObject().position).sub(this.position).normalize().multiplyScalar(velocidadRobot*delta);
			this.position.add(Direccion)
		}
		
		//Obteniendo vector de Robot->camara
		C=new THREE.Vector3().copy(controls.getObject().position);
		P=new THREE.Vector3().copy(this.position)
		Dir=new THREE.Vector3().set(C.x-P.x, C.y-P.y, C.z-P.z)
		
		//solo necesito la rotacion en Y (plano XZ)
		Rot=new THREE.Vector3().copy(Dir)
		Rot.y=0
		Rot.normalize()
		
		//Obteniendo angulo entre el frente de la grua y el ventor anterior
		X= new THREE.Vector3(1,0,0)
		//console.log(this.rotation.y)
		//X.applyAxisAngle(new THREE.Vector3(0,1,0), this.rotation.y)
		
		if(C.z<P.z)
			this.rotation.y=Rot.angleTo(X)+Math.PI
		else
			this.rotation.y=-Rot.angleTo(X)+Math.PI
		
		
		return false;
	}
	return Nuevo
}
function NuevoEnemigoTorreta(pos, rot, Distancia)
{
	var Base = Mallas["BaseTorreta"].clone()
	Base.position.set(pos[0],pos[1], pos[2])
	Base.rotation.set(rot[0], rot[1], rot[2])
	Objetos.push(Base)
	scene.add(Base)
	
	var Nuevo
	Nuevo=Mallas["Torreta"].clone();
	Nuevo.position.set(pos[0], pos[1]-6.5, pos[2])
	Nuevo.rotation.set(rot[0], rot[1], rot[2])
	
	Objetos.push(Nuevo)
	scene.add(Nuevo)
	Nuevo.LapsoED=0
	Nuevo.LapsoED2=0
	Nuevo.Estado="Inactivo"
	Nuevo.POS=0
	Nuevo.Vida=12
	Nuevo.Activador=Distancia
	
	
	Recursion(Nuevo, Nuevo)
	
	Nuevo.Impacto=function(Danio)
	{
		if(this.Vida <=0 || this.Estado!="Activo")
			return;
		
		this.Vida-=Danio;
		if(this.Vida <= 0)
		{
			//Creando explosion
			Proyectiles.push(Explosion
			(
				new THREE.Vector3().copy(this.position).add(new THREE.Vector3(0,4,0)),
				0xff8800
			))
			this.Vida=0;
			this.CDT=0;//Contador de tiempo
		}
	}
	
	Nuevo.Accion=function()
	{
		if(this.Estado=="Inactivo")
		{
			if(this.position.distanceTo(controls.getObject().position)<this.Activador)
				this.Estado="Activando"
			
				return false
		}
		else if(this.Estado=="Activando")
		{
			Temp=delta*2;
			
			this.POS+=Temp
			
			if(this.POS<6.5)
			{
				this.position.y+=Temp
				return false
			}
			else
			{
				this.Estado="Activo"
			}
		}
		
		if(this.Vida<=0)
		{
			if(this.CDT>=1)
			{
				scene.remove(this)
				return true;
			}
			this.CDT+=delta;
			return false;
		}
		
		//Obteniendo vector de torreta->camara
		var Dir, Cam, Pos
		C=new THREE.Vector3().copy(controls.getObject().position);
		P=new THREE.Vector3().copy(this.position)
		Dir=new THREE.Vector3().set(C.x-P.x, C.y-P.y, C.z-P.z)
		
		//solo necesito la rotacion en Y (plano XZ)
		Rot=new THREE.Vector3().copy(Dir)
		Rot.y=0
		Rot.normalize()
		
		//Obteniendo angulo entre el frente de la grua y el ventor anterior
		X= new THREE.Vector3(1,0,0)
		//console.log(this.rotation.y)
		//X.applyAxisAngle(new THREE.Vector3(0,1,0), this.rotation.y)
		
		if(C.z<P.z)
			this.rotation.y=Rot.angleTo(X)
		else
			this.rotation.y=-Rot.angleTo(X)
		
		//Disparo
		this.LapsoED-=delta
		this.LapsoED2-=delta
		
		if(this.LapsoED<=0)
		{
			this.LapsoED=0.8
			Dir.normalize();
			
			P.add(new THREE.Vector3(0,5.33,0))
			P.add(new THREE.Vector3().copy(Dir).multiplyScalar(8))
			
			Dir.set(C.x-P.x, C.y-P.y, C.z-P.z)
			Dir.normalize()
			
			
			Proyectiles.push(ProyectilSimple(
						P,
						Dir,
						80 ,300, 0xff0000, 1))
		}
		if(this.LapsoED2<=0)
		{
			this.LapsoED2=5
			Dir.normalize();
			
			P.add(new THREE.Vector3(0,5.33,0))
			P.add(new THREE.Vector3().copy(Dir).multiplyScalar(8))
			
			Dir.set(C.x-P.x, C.y-P.y, C.z-P.z)
			Dir.normalize()
			
			
			Proyectiles.push(ProyectilGrande(
						P,
						Dir,
						50 ,350, 0x888800, 4))
		}
		return false
	}
}
function CrearJugador(pos, rot)
{
	velocity.set(0,0,0)
	camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.01, 1000 );
	controls = new THREE.PointerLockControls( camera );
	Temp=controls.getObject()
	Temp.position.set(pos[0],pos[1],pos[2])
	Temp.rotation.set(rot[0],rot[1],rot[2])
	Temp.Raiz=Temp
	
	Colicion = new THREE.Mesh
	(
		new THREE.SphereGeometry( 3, 16, 16 ),
		MaterialTrasparente
	)

	Colicion.Raiz=Temp
	Temp.add(Colicion)
	
	Temp.Vida=10;
	Temp.Impacto=function(Danio, Direccion, Fuerza)
	{
		if(Direccion && Fuerza)
		{
			Empuje.add(new THREE.Vector3().copy(Direccion).normalize().multiplyScalar(Fuerza*delta))
		}
		this.Vida-=Danio;
		if(this.Vida<0)
			this.Vida=0;
		//alert("daño");
		
		//Reduccion de vida en la GUI
		MostrarVida(this.Vida);
		
		if(this.Vida <=0)
		{
			alert("Mueres")
			RecargarNivel=true;
		}
	}
	Objetos.push(Temp);
	scene.add(Temp)
}
function Recursion(N, Raiz)
{
	if(N.Raiz)
		return
	
	N.Raiz=Raiz
	
	for(var i=0; i<N.children.length; i++)
		Recursion(N.children[i], Raiz)
}

function CargarObjetosJSON()
{
	cargar=new THREE.JSONLoader();
	
	//---------------- Torreta
	ObjetosExistentes+=2;
	cargar.load("Modelos/Torreta/Base.json", function(geometria, materiales)
	{
		Mallas["BaseTorreta"]=new THREE.Mesh
		(
			geometria,
			M_Torreta_negro
		)
		ObjetosCargados++
		cargar.load("Modelos/Torreta/Base2.json", function(geometria)
		{
			Mallas["BaseTorreta"].add(new THREE.Mesh(
				geometria,
				M_Torreta_Brillo
			))
		})
	})
	
	ObjetosExistentes+=6;
	cargar.load("Modelos/Torreta/Rotor.json", function(geometriabase, materiales)
	{
		Mallas["Torreta"]=new THREE.Mesh
		(
			geometriabase,
			M_Torreta_negro
		)
		ObjetosCargados++
		
		cargar.load("Modelos/Torreta/Torreta.json", function(geometria, materiales)
		{
			var torreta=new THREE.Mesh
			(
				geometria,
				M_Torreta_negro
			)
			torreta.position.set(0,5.88,0)
			
			Mallas["Torreta"].add(torreta)
			Mallas["Torreta"].torreta=torreta
			
			ObjetosCargados++
			cargar.load("Modelos/Torreta/Tubo.json", function(geometria, materiales)
			{
				var tubo=new THREE.Mesh
				(
					geometria,
					M_Torreta_negro
				)
				tubo.scale.set(1.5,1,1);
				tubo.position.set(-2,0,0)
				Mallas["Torreta"].torreta.add(tubo)
				Mallas["Torreta"].torreta.tubo=tubo
				ObjetosCargados++
				
				cargar.load("Modelos/Torreta/Tubo2.json", function(geometria, materiales)
				{
					Mallas["Torreta"].torreta.tubo.add(new THREE.Mesh
					(
						geometria,
						M_Torreta_Brillo
					))
					ObjetosCargados++
				})
			})
			
			cargar.load("Modelos/Torreta/Torreta2.json", function(geometria, materiales)
			{
				Mallas["Torreta"].torreta.add(new THREE.Mesh(
					geometria,
					M_Torreta_Brillo
				))
				ObjetosCargados++
			})
			
			ObjetosCargados++
			cargar.load("Modelos/Torreta/Col.json", function(geometria, materiales)
			{
				var col=new THREE.Mesh
				(
					geometria,
					MaterialTrasparente
				)
				Mallas["Torreta"].torreta.add(col)
				Mallas["Torreta"].torreta.col=col
				ObjetosCargados++
			})
		})
		
		cargar.load("Modelos/Torreta/Rotor2.json", function(geometria, material)
		{
			Mallas["Torreta"].add(new THREE.Mesh(
				geometria,
				M_Torreta_Brillo
			))
		})
	})
	
	ObjetosExistentes+=3
	cargar.load("Modelos/Robot/Robot_Cuerpo.json", function(geometria, materiales)
	{
		Mallas["Robot"]=new THREE.Mesh
		(
			geometria,
			M_Robot_Base
		)
		ObjetosCargados++
		cargar.load("Modelos/Robot/Robot_Rojo.json", function(geometria, materiales)
		{
			Mallas["Robot"].add(new THREE.Mesh
			(
				geometria,
				M_Robot_Rojo
			))
			ObjetosCargados++
		})
		cargar.load("Modelos/Robot/Robot_Antena.json", function(geometria, materiales)
		{
			Mallas["Robot"].add(new THREE.Mesh
			(
				geometria,
				M_Robot_Antena
			))
			ObjetosCargados++
		})
	})
	ObjetosExistentes+=6
	cargar.load("Modelos/Generador/BaseNegro.json", function(geometria, materiales)//
	{
		Mallas["Generador"]= new THREE.Mesh
		(
			geometria,
			M_Generador_Negro
		)
		ObjetosCargados++
		
		cargar.load("Modelos/Generador/RotorNegro.json", function(geometria, materiales)// 0
		{
			Mallas["Generador"].add(new THREE.Mesh
			(
				geometria,
				M_Generador_Negro
			))
			ObjetosCargados++
			cargar.load("Modelos/Generador/BaseRojo.json", function(geometria, materiales)// 1
			{
				Mallas["Generador"].add(new THREE.Mesh
				(
					geometria,
					M_Generador_Rojo
				))
				ObjetosCargados++
				cargar.load("Modelos/Generador/RotorRojo.json", function(geometria, materiales)// 0/0
				{
					Mallas["Generador"].children[0].add(new THREE.Mesh
					(
						geometria,
						M_Generador_Rojo
					))
					ObjetosCargados++
					
					cargar.load("Modelos/Generador/AlasNegro.json", function(geometria, materiales)// 2
					{
						Mallas["Generador"].add(new THREE.Mesh
						(
							geometria,
							M_Generador_Negro
						))
						ObjetosCargados++
						cargar.load("Modelos/Generador/AlasRojo.json", function(geometria, materiales)// 2/1
						{
							Mallas["Generador"].children[2].add(new THREE.Mesh
							(
								geometria,
								M_Generador_Rojo
							))
							ObjetosCargados++
						})
					})
				})
			})
		})
	})
		
	ObjetosListos=false
	function Esperar()
	{
		if(ObjetosCargados >= ObjetosExistentes)
			ObjetosListos=true
		else
			window.setTimeout(Esperar, 100)
	}
	window.setTimeout(Esperar, 100)
}

function CargarNivel(Numero)
{
	Objetos=[]
	Proyectiles=[]
	
	scene = new THREE.Scene();
	light = new THREE.HemisphereLight( 0xeeeeff, 0x777788, 0.75 );
	light.position.set( 0.5, 1, 0.75 );
	
	scene.fog = new THREE.Fog( 0x000000, 0, 300 );
	scene.add(SistemaPart)
	scene.add( light );
	
	URL="Niveles/Nivel"+Numero+".php"
	request = new XMLHttpRequest()
	request.open("GET", URL, false)
	request.onreadystatechange = function(){if(request.readyState == 4)
	{
		OBJ=JSON.parse(request.responseText)
		for(i=0; i<OBJ.length; i++)
		{
			e=OBJ[i]
			if(e.tipo) switch(e.tipo)
			{
				case "Suelo":
					NuevoSuelo(e.pos, e.rot, e.dat)
				break;
				
				case "Torreta":
					NuevoEnemigoTorreta(e.pos,e.rot, e.dat.RadioDeteccion)
				break;
				
				case "Robot1":
					NuevoEnemigoRobot(e.pos, e.rot, e.dat.RadioDeteccion)
				break;
				
				case "Camara":
					CrearJugador(e.pos, e.rot)
				break;
				case "Elevador":
					NuevoElevador(e.pos, e.rot, e.dat)
					break;
				case "Nivel":
					N=new THREE.Object3D()
					N.position.set(e.pos[0], e.pos[1], e.pos[2])
					N.Accion=function()
					{
						dst=this.position.distanceTo(controls.getObject().position)
						if(dst < 12)
						{
							NivelJuego++;
							CargarNivel(NivelJuego)
						}
					}
					Objetos.push(N)
				break;
				case "Teranium":
					CrearTeranium(e.pos, e.rot, e.dat)
				break;
				case "Generador":
					CrearGenerador(e.pos, e.rot)
				break;
			}
		}
	}}
	request.send();
	
	RecargarNivel=false;
	InicializarRaton()
	Empuje.set(0,0,0)
	controls.enabled=true
	moveForward=moveLeft=moveBackward=moveRight=false;
	MostrarVida(10)
}
function MostrarVida(N)
{
	for(i=1; i<=10; i++)
		document.getElementById("V"+i).className="Vivo";
	
	for(;N<10; N++)
		document.getElementById("V"+(N+1)).className="Muerto";
}
////////////////////////////////////////////////////////////////////
var SistemaPart;
function IniciarParticulas()
{
	SistemaPart= new THREE.GPUParticleSystem({maxParticles: 40000});
	SistemaPart.tick=0;
}
function ActualizarParticulas()
{
	SistemaPart.tick+=delta;
	SistemaPart.update(SistemaPart.tick);
}
function ProyectilSimple(U, O, V ,DistanciaMaxima, Color, Danio)
{
	function Temp()
	{
		this.Accion=function()
		{
			if(!this.Explota)
			{
				Desplazamiento= new THREE.Vector3();
				Desplazamiento.copy(this.Direccion);
				Desplazamiento.multiplyScalar(delta*this.Velocidad);
				
				this.options.position.add(Desplazamiento);
				this.Distancia+=Desplazamiento.length()

				//this.options.tick+=delta;
				//SistemaPart.update(this.options.tick);
				trayectoria = new THREE.Raycaster
				(
					new THREE.Vector3().copy(this.options.position),
					new THREE.Vector3().copy(this.Direccion),
					0, 4
				);
				Cols=trayectoria.intersectObjects( Objetos, true);
				
				if(this.Distancia>=this.DistanciaMaxima)
				{
					this.Explota=true;
					this.Vida=0.0;
				}
				if(Cols.length>0)
				{
					//console.log(Cols[0].object.Raiz.Vida)
					if(Cols[0].object.Raiz)
						if(Cols[0].object.Raiz.Impacto)
							Cols[0].object.Raiz.Impacto(this.Danio, this.Direccion, 100*Danio)
					
					this.Explota=true;
					this.Vida=0.6;
					this.options.positionRandomness=1.5;
					this.options.spawnRate*=2;
					this.options.size= 40

				}
			}
			else
			{
				this.Vida-=delta;
				if(this.Vida<=0)
					return true
			}
			
			for (var x = 0; x < this.options.spawnRate * delta; x++)
				  SistemaPart.spawnParticle(this.options);
			  
			return false
		}
		this.Distancia=0
		this.Velocidad=V
		this.DistanciaMaxima=DistanciaMaxima
		this.Direccion=new THREE.Vector3()
		this.Direccion.set(O.x, O.y, O.z)
		this.Explota=false
		this.Danio=Danio
		
		this.options =
		{
			position: new THREE.Vector3(U.x, U.y, U.z),
			positionRandomness: .3,
			velocity: new THREE.Vector3(),
			velocityRandomness: .5,
			color: Color,
			colorRandomness: .9,
			turbulence: 0.0,
			lifetime: 2,
			size: 20,
			sizeRandomness: 1,
			spawnRate: 500,
			horizontalSpeed: 1.5,
			verticalSpeed: 1.33,
			timeScale: 1
		}
		
	}
	return new Temp();
}
function ProyectilGrande(U, O, V ,DistanciaMaxima, Color, Danio)
{
	function Temp()
	{
		this.Accion=function()
		{
			if(!this.Explota)
			{
				Desplazamiento= new THREE.Vector3();
				Desplazamiento.copy(this.Direccion);
				Desplazamiento.multiplyScalar(delta*this.Velocidad);
				
				this.options.position.add(Desplazamiento);
				this.Distancia+=Desplazamiento.length()

				//this.options.tick+=delta;
				//SistemaPart.update(this.options.tick);
				trayectoria = new THREE.Raycaster
				(
					new THREE.Vector3().copy(this.options.position),
					new THREE.Vector3().copy(this.Direccion),
					0, 4
				);
				Cols=trayectoria.intersectObjects( Objetos, true);
				
				if(this.Distancia>=this.DistanciaMaxima)
				{
					this.Explota=true;
					this.Vida=0.0;
				}
				if(Cols.length>0)
				{
					if(Cols[0].object.Raiz)
						if(Cols[0].object.Raiz.Impacto)
							Cols[0].object.Raiz.Impacto(this.Danio, this.Direccion, 100*Danio)
						
					this.Explota=true;
					this.Vida=0.6;
					this.options.positionRandomness=6;
					this.options.spawnRate*=8;
					this.options.size= 80
					this.options.lifetime=5;
					

				}
			}
			else
			{
				this.Vida-=delta;
				if(this.Vida<=0)
					return true
			}
			
			for (var x = 0; x < this.options.spawnRate * delta; x++)
				  SistemaPart.spawnParticle(this.options);
			  
			return false
		}
		this.Distancia=0
		this.Velocidad=V
		this.DistanciaMaxima=DistanciaMaxima
		this.Direccion=new THREE.Vector3()
		this.Direccion.set(O.x, O.y, O.z)
		this.Explota=false
		this.Danio=Danio
		
		this.options =
		{
			position: U,
			positionRandomness: 1.5,
			velocity: new THREE.Vector3(),
			velocityRandomness: .5,
			color: Color,
			colorRandomness: .4,
			turbulence: 0.0,
			lifetime: 2,
			size: 40,
			sizeRandomness: 2,
			spawnRate: 500,
			horizontalSpeed: 1.5,
			verticalSpeed: 1.33,
			timeScale: 1
		}
		
	}
	return new Temp();
}
function Explosion(U, Color)
{
	function Temp()
	{
		this.Accion=function()
		{
			for(var x = 0; x < this.options.spawnRate * delta; x++)
				SistemaPart.spawnParticle(this.options);
			
			this.Vida-=delta;
			if(this.Vida<0)
				return true;
			return false;
		}
		this.Vida=0.8;
		this.options =
		{
			position: new THREE.Vector3(U.x, U.y, U.z),
			positionRandomness: 10,
			velocity: new THREE.Vector3(),
			velocityRandomness: .5,
			color: Color,
			colorRandomness: .2,
			turbulence: 0.0,
			lifetime: 2,
			size: 60,
			sizeRandomness: 8,
			spawnRate: 800,
			horizontalSpeed: 1.5,
			verticalSpeed: 1.33,
			timeScale: 1
		}
	}
	return new Temp();
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
function Plano(P, N)
{
	this.N = new THREE.Vector3()
	this.P = new THREE.Vector3()
	this.ABCD= new THREE.Vector4()
	this.A=0;
	this.B=0;
	this.C=0;
	this.D=0;
	
	this.N.copy(N)
	this.P.copy(P)
	
	this.Actualizar= function()
	{
		this.A=N.x
		this.B=N.y
		this.C=N.z
		this.D=-(-((N.x*P.x) + (N.y+P.y) + (N.z+P.z)));
	}
	this.Actualizar();
	this.Distancia = function(P)
	{
		Mul=this.A*P.x + this.B*P.y + this.C*P.z + this.D
		Pun=Math.sqrt(this.A*this.A + this.B*this.B + this.C*this.C)
		
		if(Pun != 0)
			return Mul/Pun
		
		return 0
	}
}
function Coliciones()
{
	this.Esfera = function(Afectado, Radio)
	{
		RecibirColicion=function( Objeto )
		{
			
		}
		EnviarColicion=function( Objeto )
		{
			Direccion = new THREE.Vector3().copy(Objeto.position).sub(this.position)
			if(Direccion.length()<Radio)
			{
				Direccion.normalize()
			}
		}
	}
}