[
	{
		"nombre":"Personaje",
		"tipo":"Camara",
		"pos":[-42,25,-20],
		"rot":[0,3.14,0],
		"esc":[1,1,1],
		"dat":{}
	},
	{
		"nombre":"Inicio",
		"tipo":"Suelo",
		"pos":[-42,17,-20],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":40, "Y":2, "Z":80}
	},
	{
		"nombre":"Primer escalon",
		"tipo":"Suelo",
		"pos":[-19,15,0],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":6, "Y":2, "Z":40}
	},
	{
		"nombre":"Segundo escalon",
		"tipo":"Suelo",
		"pos":[-13,13,0],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":6, "Y":2, "Z":40}
	},
	{
		"nombre":"Tercer escalon",
		"tipo":"Suelo",
		"pos":[-7,11,0],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":6, "Y":2, "Z":40}
	},
	{
		"nombre":"Cuarto esalon",
		"tipo":"Suelo",
		"pos":[-1,9,0],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":6, "Y":2, "Z":40}
	},
	
	
	
	{
		"nombre":"Plataforma1/1",
		"tipo":"Suelo",
		"pos":[31,7,-70],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma1/2",
		"tipo":"Suelo",
		"pos":[31,7,-10],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma1/3",
		"tipo":"Suelo",
		"pos":[31,7,50],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma1/4",
		"tipo":"Suelo",
		"pos":[91,7,-70],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma1/5",
		"tipo":"Suelo",
		"pos":[91,7,-10],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma1/6",
		"tipo":"Suelo",
		"pos":[91,7,50],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma1/7",
		"tipo":"Suelo",
		"pos":[150,7,-10],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma1/8",
		"tipo":"Suelo",
		"pos":[211,7,-10],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma1/8",
		"tipo":"Suelo",
		"pos":[211,7,50],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma1/9",
		"tipo":"Suelo",
		"pos":[253,6,-55],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma1/10",
		"tipo":"Suelo",
		"pos":[258,6,-90],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma1/11",
		"tipo":"Suelo",
		"pos":[280,8,-69],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	
	
	
	{
		"nombre":"PreObstaculo",
		"tipo":"Suelo",
		"pos":[341,9,-80],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":80, "Y":2, "Z":40}
	},
	{
		"nombre":"Obstaculo2",
		"tipo":"Suelo",
		"pos":[408,15,-50],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":40, "Y":2, "Z":100}
	},
	{
		"nombre":"Obstaculo3",
		"tipo":"Suelo",
		"pos":[446,17,22],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":100, "Y":2, "Z":25}
	},
	{
		"nombre":"MiniPlataforma/12",
		"tipo":"Suelo",
		"pos":[451,17,-6],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"MiniPlataforma/13",
		"tipo":"Suelo",
		"pos":[482,21,46],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	


	{
		"nombre":"Plataforma2/1",
		"tipo":"Suelo",
		"pos":[536,22,39],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma2/2",
		"tipo":"Suelo",
		"pos":[536,24,99],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma2/3",
		"tipo":"Suelo",
		"pos":[596,24,39],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma2/4",
		"tipo":"Suelo",
		"pos":[596,24,99],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma2/5",
		"tipo":"Suelo",
		"pos":[656,24,39],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma2/6",
		"tipo":"Suelo",
		"pos":[656,24,99],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma2/7",
		"tipo":"Suelo",
		"pos":[652,24,139],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma2/8",
		"tipo":"Suelo",
		"pos":[672,24,139],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma2/9",
		"tipo":"Suelo",
		"pos":[692,24,139],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma2/10",
		"tipo":"Suelo",
		"pos":[696,24,38],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma2/11",
		"tipo":"Suelo",
		"pos":[714,24,66],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma2/12",
		"tipo":"Suelo",
		"pos":[727,26,38],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma2/13",
		"tipo":"Suelo",
		"pos":[758,29,50],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma2/14",
		"tipo":"Suelo",
		"pos":[778,35,73],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma2/15",
		"tipo":"Suelo",
		"pos":[776,24,129],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	
	

	{
		"nombre":"Puente",
		"tipo":"Suelo",
		"pos":[805,29,242],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":4, "Y":2, "Z":150}
	},
	{
		"nombre":"Trampa",
		"tipo":"Suelo",
		"pos":[855,28,315],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":10, "Y":2, "Z":10}
	},
	
	
	{
		"nombre":"Plataforma3/1",
		"tipo":"Suelo",
		"pos":[781,29,356],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma3/2",
		"tipo":"Suelo",
		"pos":[781,29,416],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma3/3",
		"tipo":"Suelo",
		"pos":[721,29,345],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma3/4",
		"tipo":"Suelo",
		"pos":[721,31,405],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma3/5",
		"tipo":"Suelo",
		"pos":[721,31,465],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma3/6",
		"tipo":"Suelo",
		"pos":[661,31,422],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma3/7",
		"tipo":"Suelo",
		"pos":[605,35,351],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	{
		"nombre":"Plataforma3/8",
		"tipo":"Suelo",
		"pos":[559,39,351],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma3/8",
		"tipo":"Suelo",
		"pos":[523,43,359],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma3/9",
		"tipo":"Suelo",
		"pos":[523,28,404],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":20}
	},
	{
		"nombre":"Plataforma3/10",
		"tipo":"Suelo",
		"pos":[511,29,446],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":60}
	},
	
	

	{
		"nombre":"Escalonfeo 1",
		"tipo":"Suelo",
		"pos":[517,31,484],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":8, "Y":2, "Z":3.5}
	},
	{
		"nombre":"Escalonfeo 2",
		"tipo":"Suelo",
		"pos":[517,33,491],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":8, "Y":2, "Z":3.5}
	},
	{
		"nombre":"Escalonfeo 3",
		"tipo":"Suelo",
		"pos":[517,35,499],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":8, "Y":2, "Z":3.5}
	},
	{
		"nombre":"Escalonfeo final",
		"tipo":"Elevador",
		"pos":[517,34,527],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":2, "Z":15, "Max": 300}
	},
	{
		"nombre":"Fin del nivel",
		"tipo":"Nivel",
		"pos":[517,270,527],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"Fin":0}
	},
	
	
	
	{
		"nombre":"Robot1 P1",
		"tipo":"Robot1",
		"pos":[109,8,54],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{}
	},
	{
		"nombre":"Robot2 P1",
		"tipo":"Robot1",
		"pos":[213,8,-6],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{}
	},
	{
		"nombre":"Robot1 P2",
		"tipo":"Robot1",
		"pos":[485,18,23],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{}
	},
	{
		"nombre":"Robot1 P3",
		"tipo":"Robot1",
		"pos":[558,25,86],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{}
	},
	{
		"nombre":"Robot2 P3",
		"tipo":"Robot1",
		"pos":[664,25,142],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{}
	},
	{
		"nombre":"Robot2 P3",
		"tipo":"Robot1",
		"pos":[797,25,151],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{}
	},
	{
		"nombre":"Robot1 P4",
		"tipo":"Robot1",
		"pos":[714,29,342],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"Oculto":"true", "RadioDeteccion":180}
	},
	
	
	
	{
		"nombre":"Torreta1 P1",
		"tipo":"Torreta",
		"pos":[314,10,-80],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"Oculto":"true", "RadioDeteccion":180}
	},
	{
		"nombre":"Torreta1 P2",
		"tipo":"Torreta",
		"pos":[855,29,315],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"Oculto":"true", "RadioDeteccion":300}
	},
	{
		"nombre":"Torreta1 P2",
		"tipo":"Torreta",
		"pos":[520,30,468],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"Oculto":"true", "RadioDeteccion":300}
	}
]