[
	{
		"nombre":"Personaje",
		"tipo":"Camara",
		"pos":[0,3,0],
		"rot":[0,-1.6,0],
		"esc":[1,1,1],
		"dat":{}
	},
	{
		"nombre":"Inicio",
		"tipo":"Elevador",
		"pos":[0,0,0],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":2, "Z":15, "Max": 100}
	},
	{
		"nombre":"Piso1/1",
		"tipo":"Suelo",
		"pos":[-15,100,0],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":2, "Z":15}
	},
	{
		"nombre":"Piso1/2",
		"tipo":"Suelo",
		"pos":[15,100,0],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":2, "Z":15}
	},
	{
		"nombre":"Piso1/3",
		"tipo":"Suelo",
		"pos":[0,100,15],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":2, "Z":15}
	},
	{
		"nombre":"Piso1/4",
		"tipo":"Suelo",
		"pos":[0,100,-15],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":2, "Z":15}
	},
	{
		"nombre":"Piso2/1",
		"tipo":"Suelo",
		"pos":[15,102,-15],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":2, "Z":15}
	},
	{
		"nombre":"Piso2/2",
		"tipo":"Suelo",
		"pos":[15,102,15],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":2, "Z":15}
	},
	{
		"nombre":"Piso2/3",
		"tipo":"Suelo",
		"pos":[-15,102,15],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":2, "Z":15}
	},
	{
		"nombre":"Piso2/4",
		"tipo":"Suelo",
		"pos":[-15,102,-15],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":2, "Z":15}
	},
	
	
	{
		"nombre":"Piso3/1",
		"tipo":"Suelo",
		"pos":[-43,102,0],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":41, "Y":2, "Z":15}
	},
	{
		"nombre":"Piso3/2",
		"tipo":"Suelo",
		"pos":[43,102,0],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":41, "Y":2, "Z":15}
	},
	
	
	{
		"nombre":"Piso4/1",
		"tipo":"Suelo",
		"pos":[-54,104,36],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":40}
	},
	{
		"nombre":"Piso4/2",
		"tipo":"Suelo",
		"pos":[54,104,36],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":40}
	},
	{
		"nombre":"Piso4/3",
		"tipo":"Suelo",
		"pos":[-54,104,-36],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":40}
	},
	{
		"nombre":"Piso4/4",
		"tipo":"Suelo",
		"pos":[54,104,-36],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":20, "Y":2, "Z":40}
	},
	{
		"nombre":"Piso4/5",
		"tipo":"Suelo",
		"pos":[0,100,-53],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":8}
	},
	{
		"nombre":"Piso4/6",
		"tipo":"Suelo",
		"pos":[0,100,53],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":60, "Y":2, "Z":8}
	},
	
	{
		"nombre":"Base 1",
		"tipo":"Suelo",
		"pos":[80,104,82],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":5, "Z":15}
	},
	{
		"nombre":"Base 2",
		"tipo":"Suelo",
		"pos":[-80,104,82],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":5, "Z":15}
	},
	{
		"nombre":"Base 3",
		"tipo":"Suelo",
		"pos":[80,104,-82],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":5, "Z":15}
	},
	{
		"nombre":"Base 4",
		"tipo":"Suelo",
		"pos":[-80,104,-82],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"X":15, "Y":5, "Z":15}
	},
	
	
	{
		"nombre":"Torreta1",
		"tipo":"Torreta",
		"pos":[80,106.8,82],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"Oculto":"true", "RadioDeteccion":115}
	},
	{
		"nombre":"Torreta2",
		"tipo":"Torreta",
		"pos":[-80,106.8,82],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"Oculto":"true", "RadioDeteccion":115}
	},
	{
		"nombre":"Torreta3",
		"tipo":"Torreta",
		"pos":[-80,106.8,-82],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"Oculto":"true", "RadioDeteccion":115}
	},
	{
		"nombre":"Torreta3",
		"tipo":"Torreta",
		"pos":[80,106.8,-82],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"Oculto":"true", "RadioDeteccion":115}
	},
	{
		"nombre":"Enemigo",
		"tipo":"Teranium",
		"pos":[59,106,0],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{"Oculto":"true", "RadioDeteccion":50}
	},
	{
		"nombre":"Generador",
		"tipo":"Generador",
		"pos":[200,110,0],
		"rot":[0,0,0],
		"esc":[1,1,1],
		"dat":{}
	}
]